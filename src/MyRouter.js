import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import SignIn from "./pages/SignIn";
import SignUp from "./pages/SignUp";
import Oauth2Handler from "./pages/Oauth2Handler";
import Cabinet from "./components/Cabinet";
import EditProfile from "./pages/EditProfile";

const MyRouter = () => {
    return (
        <Router>
            <Routes>
                <Route
                    path={"/sign-in"}
                    element={<SignIn/>}/>
                <Route
                    path={"/"}
                    element={<SignIn/>}/>
                <Route
                    path={"/sign-up"}
                    element={<SignUp/>}/>
                <Route
                    path={"/oauth2/redirect"}
                    element={<Oauth2Handler/>}/>
                <Route
                    path={"/cabinet"}
                    element={<Cabinet/>}/>

                <Route
                    path={"/cabinet/edit-profile"}
                    element={<EditProfile/>}/>
            </Routes>
        </Router>
    );
}

export default MyRouter;
