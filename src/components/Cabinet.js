import {useEffect, useState} from "react";
import {ACCESS_TOKEN} from "../utils/RestContants";
import {Button} from "reactstrap";

const Cabinet = () => {

    useEffect(() => {
            if (!localStorage.getItem(ACCESS_TOKEN)){
                window.location.replace("/sign-in")
            }
        },
        []
    )
    return (
        <div>
            Cabinet page ga xush kelibsiz
            <button onClick={() => {
                localStorage.removeItem("AccessToken")
                localStorage.removeItem("RefreshToken");
                window.location.replace('/')
            }}>Logout
            </button>
            <br/>
            <Button
                color={"primary"}
                onClick={()=>{
                window.location.replace("/cabinet/edit-profile")
            }}>Edit profile</Button>
        </div>
    )
}

export default Cabinet;