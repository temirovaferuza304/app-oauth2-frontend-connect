import axios from "axios";
import {useState} from "react";
import {Link} from "react-router-dom";
import {ACCESS_TOKEN} from "../utils/RestContants";

const SignInComponent = ({}) => {
    const [phoneNumber, setPhoneNumber] = useState('');
    const [password, setPassword] = useState('');

    const login = () => {
        let req = {password, phoneNumber};
        axios.post(
            "http://localhost:80/api/auth/sign-in",
            req)
            .then(res => {
                localStorage.setItem('AccessToken', res.data.data.tokenType + res.data.data.accessToken);
                localStorage.setItem('RefreshToken', res.data.data.refreshToken);
                window.location.replace('/cabinet')
            })
            .catch(err => console.log(err))
    }

    return (
        <div>
            <h4>Login</h4>
            <br/>
            <label htmlFor="phoneNumber">Phone number</label>
            <br/>
            <input
                id="phoneNumber"
                type="text"
                placeholder="Enter phone number"
                onChange={(e) => setPhoneNumber(e.target.value)}
            />
            <br/>
            <label htmlFor="password">Password</label>
            <br/>
            <input
                id="password"
                type="password"
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
            />
            <br/>
            <button onClick={login}>Login</button>
            <p>New user?</p>
            <Link to={"/sign-up"}>SignUp</Link>
        </div>
    )
}

export default SignInComponent;