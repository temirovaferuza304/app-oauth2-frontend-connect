import React from "react";
import fbLogo from '../assets/icons/fb-logo.png';
import googleLogo from '../assets/icons/google-logo.png';
import githubLogo from '../assets/icons/github-logo.png';
import {FACEBOOK_SIGN_URL, GITHUB_SIGN_URL, GOOGLE_SIGN_URL} from "../utils/api";
import '../pages/index.css'
import '../index.css'

const SocialLoginComponent = ({
                                  googleUrl,
                                  facebookUrl,
                                  githubUrl,
                                  text
                              }) => {
    return (
        <div className="social-login">
            <a
                className="btn btn-block social-btn google"
                href={googleUrl}>
                <img src={googleLogo} alt="Google"/>{text}</a>
            <a
                className="btn btn-block social-btn facebook"
                href={facebookUrl}>
                <img src={fbLogo} alt="Facebook"/>{text}</a>
            <a
                className="btn btn-block social-btn github"
                href={githubUrl}>
                <img src={githubLogo} alt="Github"/>{text}</a>
        </div>
    )
}

export default SocialLoginComponent;