import SignInComponent from "../components/SignInComponent";
import SocialLoginComponent from "../components/SocialLoginComponent";
import {useLocation} from "react-router-dom";
import {useEffect, useState} from "react";
import {FACEBOOK_SIGN_URL, GITHUB_SIGN_URL, GOOGLE_SIGN_URL} from "../utils/api";

const SignIn = () => {
    const [errorMessage, setErrorMessage] = useState('');
    useEffect(() => {
        if (errorMessage)
            alert(errorMessage)
    }, [errorMessage])


    const useQuery = () => {
        return new URLSearchParams(useLocation().search);
    }

    const msg = useQuery().get("error");//sign-in?error=sad;aj


    useEffect(() => {
        if (msg) {
            setErrorMessage(msg)
        }
    }, [])


    return (
        <>
            <div className="login-container">
                <div className="login-content">
                    <h1 className="login-title">Login to SpringSocial</h1>
                    <SocialLoginComponent
                        text={"Login with"}
                        googleUrl={GOOGLE_SIGN_URL}
                        facebookUrl={FACEBOOK_SIGN_URL}
                        githubUrl={GITHUB_SIGN_URL}
                    />
                    <div className="or-separator">
                        <span className="or-text">OR</span>
                    </div>
                    <SignInComponent/>
                </div>
            </div>
        </>
    )
}

export default SignIn;